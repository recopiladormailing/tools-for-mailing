// import packages

var Client = require('ftp'),
    fs     = require('fs');

var c = new Client();

var connectionProperties = {
	host : '10.12.6.101',
	user : 'mailing',
	password : 'M41lPh4n101'
}

var folder = "NUEVO-MAILING";

var images = ['image00.jpg', 'image01.jpg', 'image02.jpg', 'image03.jpg', 'image04.jpg'];

c.on('ready', function() {
	console.log('Connection ready');
  
  c.mkdir(folder, function (err) {
  	if( err ) throw err;
  	console.log('Folder : ' + folder + ' is created');
  });

  for ( var image in images) {
    var file = images[image];
    c.put(file, folder + '/' + file, function (err) {
      if ( err ) throw err;
      console.log('File : '+ file + ' uploaded');
    });  
  }

  c.end();

});
c.connect(connectionProperties);