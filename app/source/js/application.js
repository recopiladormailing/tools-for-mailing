
function allowDrop (event) {
		event.preventDefault();
}

function drag (event) {
	event.dataTransfer.setData("Text", event.target.id);
}

function drop(event) {
	event.preventDefault();
	var data = event.dataTransfer.getData("Text");
	var $img = document.querySelector("#"+data);
	var height, width;
	height = $img.offsetHeight;
	width = $img.offsetWidth;
	var $tr = document.createElement('tr');
	$tr.setAttribute('height', height);
	var $td = document.createElement('td');
	$td.appendChild($img);
	$td.setAttribute('height', height);
	$td.setAttribute('width', width);
	$tr.appendChild($td);
	event.target.appendChild($tr);
}

(function() {
	var containerFiles = document.querySelector('.containerFiles');
	var $files = document.querySelector('#files');
	$files.addEventListener('change', loadImage, false);
	function loadImage(evt) {
		console.log(evt);
		var files = evt.target.files;
		for (var i = 0, f; f = files[i]; i++) {
			console.log(f);
			if (!f.type.match('image*')) {
				continue;
			}

			var reader = new FileReader();
			var $p = document.createElement('p');
			$p.setAttribute('class','image');
			reader.onload = (function(theFile){
				return function(e) {
					$p.innerHTML = ['<img class="thumb" draggable="true" ondragstart="drag(event)" src="',
													e.target.result,'" title="', escape(theFile.name),'" id="image0',i ,'"/>'].join('');
					containerFiles.appendChild($p);					
				}
			})(f);

			reader.readAsDataURL(f);
		}
	}	

}).call(this);