module.exports = function (grunt) {
	// Project configuration
	grunt.initConfig({
		
		// read package.json to load dependencies
		pkg: grunt.file.readJSON('package.json'),

		/* 
			If you want to generate a debug file and a release
			file from the same template.
		*/
		// Configure Jade
		jade: {
			debug: {
				options: {
					pretty: true
				},
				files: [
					{
						expand: true,
						cwd: 'source',
						dest: 'source',
						src: '*.jade',
						ext: '.html' 
					}
				]
			},
			release: {
				files: [
					{
						expand: true,
						cwd: 'source',
						dest: 'dist',
						src: '*.jade',
						ext: '.html'
					}
				]
			}
		},
		// Configure Stylus
		stylus: {
			debug: {
				options: {
					compress: true
				},
				files: {
					'source/css/style.min.css': 'source/stylus/style.styl'
				}
			},
			release: {
				files: {
					'dist/css/style.min.css': 'source/stylus/style.styl'
				}
			}
		},

		uglify: {
			dist: {
				src: 'source/js/application.js',
				dest: 'dist/js/application.min.js'
			}
		},

		watch: {
			jadewatch: {
				files: ['source/*.jade', 'source/jade/layouts/*.jade'],
				tasks: ['jade']
			},
			stylewatch: {
				files: ['source/stylus/*styl', 'source/stylus/*/*.styl'],
				tasks: ['stylus'],
				options: {
					livereload: true
				}
			},
			uglifywatch: {
				files: ['source/js/application.js'],
				tasks: ['uglify']
			}
		}
	});

	// Load plugins with Npm
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-jade');
	grunt.loadNpmTasks('grunt-contrib-stylus');
	grunt.loadNpmTasks('grunt-contrib-uglify');

	// Register tasks
	grunt.registerTask('default', ['watch']);
};